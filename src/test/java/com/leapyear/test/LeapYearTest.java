package com.leapyear.test;

import com.leapyear.LeapYear;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LeapYearTest {

    public LeapYear calender;

    @Before
    public void setUp() {
        calender = new LeapYear();
    }


    @Test
    public void whenDivideBy400ThenLeapYear() {
        assertTrue(calender.yearDivisibleBy400(2000));
    }

    @Test
    public void whenDivideby100ButNotBy400ThenNotLeapYear() {
        assertTrue(calender.yearDivisibleBy100ButNotBy400(1700));
        assertTrue(calender.yearDivisibleBy100ButNotBy400(1800));
        assertTrue(calender.yearDivisibleBy100ButNotBy400(1900));
        assertTrue(calender.yearDivisibleBy100ButNotBy400(2100));
    }

    @Test
    public void whenDivideBy4ButNotBy100ThenLeapYear() {
        assertTrue(calender.divisibleBy4ButNotBy100(2008));
        assertTrue(calender.divisibleBy4ButNotBy100(2012));
        assertTrue(calender.divisibleBy4ButNotBy100(2016));
    }

    @Test
    public void givenNotDividedBy4ThenNotLeapYear() {
        assertTrue(calender.notDivisibleBy4(2017));
        assertTrue(calender.notDivisibleBy4(2018));
        assertTrue(calender.notDivisibleBy4(2019));
    }

    @Test
    public void givenLeapYearsPriorTo1582() {
        assertTrue(calender.isLeapYear(2008));
        assertTrue(calender.isLeapYear(2012));
        assertTrue(calender.isLeapYear(2016));
        assertTrue(calender.isLeapYear(2000));
        assertTrue(calender.isLeapYear(2044));
    }

    @Test
    public void givenNonLeapYearsPriorTo1582() {
        assertFalse(calender.isLeapYear(2017));
        assertFalse(calender.isLeapYear(2018));
        assertFalse(calender.isLeapYear(2019));
        assertFalse(calender.isLeapYear(2100));

    }

}
