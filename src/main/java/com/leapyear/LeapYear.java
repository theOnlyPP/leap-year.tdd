package com.leapyear;

import java.util.Objects;

import static java.util.Objects.equals;

public class LeapYear {

    public boolean isLeapYear(int year) {

        return (yearDivisibleBy400(year) || !yearDivisibleBy100ButNotBy400(year))
                &&
                (divisibleBy4ButNotBy100(year) || !notDivisibleBy4(year));
    }


    public boolean yearDivisibleBy400(int year) {
        return Objects.equals(year % 400, 0);

    }

    public boolean yearDivisibleBy100ButNotBy400(int year) {
        return divisibleBy100(year) && !yearDivisibleBy400(year);
    }

    public boolean divisibleBy4ButNotBy100(int year) {
        return divisibleBy4(year) && !divisibleBy100(year);
    }

    public boolean notDivisibleBy4(int year) {
        return !divisibleBy4(year);
    }

    private boolean divisibleBy4(int year) {
        return Objects.equals(year % 4, 0);
    }

    private boolean divisibleBy100(int year) {
        return Objects.equals(year % 100,0);
    }
}
