Tools/Setups required:
Java 8
Maven (Please make sure they are system path) 
Note: Use `mvn -v` to check installed maven and `java -version` to check java

Steps to run:
1) Navigate inside project, locate file called pom.xml
2) Open cmd/Terminal where pom.xml is located, Type `mvn clean test`
3) Output will be displayed inside console/terminal. If not please check file named prefixed with `TEST` inside `src/target/surfire-reports`
4) If any of step fails, please make sure your java version is compatible with java version `1.8.0_241` (build 1.8.0_241-b07) and maven (3.6.3)

Thanks